package ru.devray.day11;

public class UsefulUtil {

    public int add(int a, int b) {
        return a + b;
    }

    public double divide(int a, int b) {
        if (b == 0) {
            throw new RuntimeException("делишь на ноль, дурачина!");
        }
        return (double) a / b;
    }

    public String addSquareBracketsToStartAndEnd(String input) {
//        return String.format("[%s %s]", input, "abc");
        return "[%s]".formatted(input);
    }




}
