package ru.devray.day11.bitcoinpayment;

import org.testng.annotations.*;
import org.testng.asserts.Assertion;
import ru.devray.day11.UsefulUtil;

public class CopiedUsefulUtilTest {

    UsefulUtil usefulUtil;

    @BeforeClass
    public void setUpClass() {
        System.out.println("in @BeforeClass");
    }

    @BeforeMethod
    public void setUp() {
        System.out.println("in @BeforeMethod");
        usefulUtil = new UsefulUtil();
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("in @AfterMethod");
        usefulUtil = new UsefulUtil();
    }

    @Test
    public void textBracketsMethod() {
        String expectedResult = "[1234]";
        String actualResult = usefulUtil.addSquareBracketsToStartAndEnd("1234");

        Assertion assertion = new Assertion();
        assertion.assertEquals(actualResult, expectedResult);
    }

    @Test(dataProvider = "numbersProvider")
    public void testAddition(int a, int b, int expectedResult) {
        System.out.println("in @Test");

        int actualResult = usefulUtil.add(a, b);

        Assertion assertion = new Assertion();
        assertion.assertEquals(actualResult, expectedResult, "Некорректный результат сложения!");
    }
    @Test(dataProvider = "numbersProvider")
    public void testAddition(int a, String b, int expectedResult) {
        System.out.println("in @Test");

        int actualResult = usefulUtil.add(a, 1);

        Assertion assertion = new Assertion();
        assertion.assertEquals(actualResult, expectedResult, "Некорректный результат сложения!");
    }

    //Object
    @DataProvider(name = "numbersProvider")
    public Object[][] dataProvider(){
        return new Object[][]{
                {2, 2, 4},
                {-10, -20, -30},
                {-10, -20, -29},
                {-10, 20, 10}
        };
    }
    //Object
    @DataProvider(name = "objectsProvider")
    public Object[][] dataProvider2(){
        return new Object[][]{
                {2, "ab", "abab"}
        };
    }

}
