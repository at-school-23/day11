package ru.devray.day11;

import com.beust.ah.A;
import org.testng.Assert;
import org.testng.annotations.*;
import org.testng.asserts.Assertion;

import java.io.IOException;

public class UsefulUtilTest {

    UsefulUtil usefulUtil;

    @BeforeClass
    public void setUpClass() {
        System.out.println("in @BeforeClass");
    }

    @BeforeMethod
    public void setUp() {
        System.out.println("in @BeforeMethod");
        usefulUtil = new UsefulUtil();
    }

    @AfterMethod
    public void tearDown() {
        System.out.println("in @AfterMethod");
        usefulUtil = new UsefulUtil();
    }

    @Test
    public void textBracketsMethod() {
        String expectedResult = "[1234]";
        String actualResult = usefulUtil.addSquareBracketsToStartAndEnd("1234");

        Assert.assertEquals(actualResult, expectedResult);
    }

    @Test
    public void testAddition() {
        System.out.println("in @Test");

        int expectedResult = 4;
        int actualResult = usefulUtil.add(2, 2);

        Assert.assertEquals(actualResult, expectedResult, "Некорректный результат сложения!");
    }

    @Test(groups = {"smoke", "ui", "payment"})
    public void testAdditionOfNegativeNumbers() {
        System.out.println("in @Test");
        int expectedResult = -30;
        int actualResult = usefulUtil.add(-10, -20);

        Assert.assertEquals(actualResult, expectedResult, "Некорректный результат сложения!");
    }

    @Test
    public void testAdditionOfPostitveAndNegativeNumbers() {
        System.out.println("in @Test");
        int expectedResult = -10;
        int actualResult = usefulUtil.add(10, -20);

        Assert.assertEquals(actualResult, expectedResult, "Некорректный результат сложения!");
    }

    @Test
    public void testDivide() {
        double actualResult = usefulUtil.divide(14, 2);
        double expectedResult = 7;

        Assert.assertEquals(actualResult, expectedResult);
    }

    @Test
    public void testDivideNegative() {
        if (true) throw new RuntimeException();
        Assert.assertThrows(IOException.class, () -> usefulUtil.divide(14, 0));
    }

}
